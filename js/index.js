var video = document.querySelector("#videoElement");

/*
    Start the stream
*/
function startStream() {

    if (navigator.mediaDevices.getUserMedia) {
        navigator.mediaDevices.getUserMedia({
                video: true
            })
            .then(function(stream) {
                video.srcObject = stream;
            })
            .catch(function(err0r) {
                console.log("Something went wrong!");
            });
    }

}

//Bouton pour stopper le stream
function stopStream() {
    var stream = video.srcObject;
    var tracks = stream.getTracks();

    for (var i = 0; i < tracks.length; i++) {
        var track = tracks[i];
        track.stop();
    }

    video.srcObject = null;
}

// Fonction qui decoche les checkbox de la meme catégorie
$('.hairCheckbox').click(function() {
    var checkedState = $(this).prop("checked");
    $(this)
        .parent('form')
        .children('.hairCheckbox:checked')
        .prop("checked", false);

    $(this).prop("checked", checkedState);
});


/////////////WebSocket Management/////////////

var ws = null;

try {
    ws = new WebSocket("ws://" + location.host);
} catch (error) {
    console.log(error);
}

/*
    Sends data when the connection is up
*/
ws.sendData = function(data) {
    // Wait until the state of the socket is not ready and send the message when it is...
    ws.waitForConnection(function() {
        ws.send(data);
    });
};

/*
    Waits for the websocket connection to be initialized, then proceeds to call a defined callback function
*/
ws.waitForConnection = function(callback) {
    setTimeout(
        function() {
            if (ws.readyState === 1) {
                if (callback != null) {
                    callback();
                }
            } else {
                console.log("wait for connection...");
                ws.waitForConnection(callback);
            }

        }, 5); // wait 5 milisecond for the connection...
};


ws.onerror = function(error) {
    console.error(error);
};

ws.onopen = function(event) {
    console.log("Connexion établie.");

    // End of connection
    this.onclose = function(event) {
        console.log("Connexion terminée.");
    };
};

ws.onmessage = function(event) {
    console.log("Message:", event.data);
};

while (video.srcObject != null) {
    ws.sendData(video.srcObject.stream);
}