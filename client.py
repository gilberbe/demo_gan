#!/usr/bin/python3

import asyncio
import websockets
from sys import argv


if len(argv) == 2:
    PORT = int(argv[1])
else:
    PORT = 8080

async def hello():
    uri = "ws://localhost:{}".format(PORT)
    async with websockets.connect(uri) as websocket:
        name = input("What's your name? ")

        await websocket.send(name)
        print(f"> {name}")

        greeting = await websocket.recv()
        print(f"< {greeting}")

asyncio.get_event_loop().run_until_complete(hello())